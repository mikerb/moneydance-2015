/*
 *  Copyright (c) 2014, Michael Bray. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - The name of the author may not used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 
package com.moneydance.modules.features.loadsectrans;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.SortedSet;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import com.infinitekind.moneydance.model.AbstractTxn;
import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.OnlineTxn;
import com.infinitekind.moneydance.model.ParentTxn;
import com.infinitekind.moneydance.model.SplitTxn;

public class GenerateWindow extends JPanel {
	private SortedSet<SecLine> setLine;
	private Account acct;
	private GenerateTable transTab;
	private GenerateTableModel transModel;
	private JPanel panTop;
	private JPanel panMid;
	private JPanel panBot;
	private JTextField txtAccount;
	private JCheckBox jcSelect;
	private JButton btnClose;
	private JButton btnSave;

	private JScrollPane spTrans;
	private Parameters objParms;
	public GenerateWindow(SortedSet<SecLine> setLinep, Account acctp, Parameters objParmsp) {
		setLine = setLinep;
		acct = acctp;
		objParms = objParmsp;
		transModel = new GenerateTableModel();
		transTab = new GenerateTable(transModel);
		generateTrans();
		/*
		 * Start of screen
		 * 
		 * Top Panel Account
		 */
		this.setLayout(new BorderLayout());
		panTop = new JPanel (new GridBagLayout());
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.gridx = 0;
		gbc_label.gridy = 0;
		JLabel lbAccount = new JLabel("Investment Account:");
		panTop.add(lbAccount,gbc_label);
		GridBagConstraints gbc_account = new GridBagConstraints();
		gbc_account.gridx = 1;
		gbc_account.gridy = 0;
		txtAccount = new JTextField(acct.getAccountName());
		panTop.add(txtAccount,gbc_account);
		this.add(panTop,BorderLayout.PAGE_START);
		/*
		 * Middle Panel table
		 */
		panMid = new JPanel ();
		panMid.setLayout(new BoxLayout(panMid,BoxLayout.Y_AXIS));
		spTrans = new JScrollPane (transTab);
		spTrans.setAlignmentX(LEFT_ALIGNMENT);
		panMid.add(spTrans,BorderLayout.LINE_START);
		spTrans.setPreferredSize(new Dimension(Constants.LOADSCREENWIDTH,Constants.LOADSCREENHEIGHT));
		jcSelect = new JCheckBox();
		jcSelect.setAlignmentX(LEFT_ALIGNMENT);
		jcSelect.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				boolean bNewValue;
				if (e.getStateChange() == ItemEvent.DESELECTED)
					bNewValue = false;
				else
					bNewValue = true;
				for (int i=0;i<transModel.getRowCount();i++)
					transModel.setValueAt(bNewValue, i, 0);
				transModel.fireTableDataChanged();
			}
		});
		panMid.add(jcSelect);
		this.add(panMid,BorderLayout.CENTER);
		
		
		/*
		 * Add Buttons
		 */
		panBot = new JPanel(new GridBagLayout());
		/*
		 * Button 1
		 */
		GridBagConstraints gbcbt1 = new GridBagConstraints();
		gbcbt1.gridx = 0;
		gbcbt1.gridy = 0;
		gbcbt1.anchor = GridBagConstraints.LINE_START;
		gbcbt1.insets = new Insets(15,15,15,15);
		btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		panBot.add(btnClose,gbcbt1);

		/*
		 * Button 2
		 */
		GridBagConstraints gbcbt2 = new GridBagConstraints();
		gbcbt2.gridx = gbcbt1.gridx+1;
		gbcbt2.gridy = gbcbt1.gridy;
		gbcbt2.insets = new Insets(15,15,15,15);
		btnSave = new JButton("SaveTransactions");
		btnSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				save();
				panMid.invalidate();
				panMid.validate();
			}
		});
		panBot.add(btnSave,gbcbt2);
		
		this.add(panBot,BorderLayout.PAGE_END);
			
	}
	private void generateTrans() {
		for (SecLine objLine :setLine) {
			if (!objLine.getSelect())
				continue;
			FieldLine objMatch = objParms.matchType(objLine.getReference());
			if (objMatch != null) {
				switch (Constants.TRANSTYPES[objMatch.getTranType()]) {
				case Constants.SECURITY_INCOME :
					transModel.addLine(new GenerateTransaction(Constants.PARENT,acct,objLine.getDate(),
							objLine.getValue(),"",OnlineTxn.INVEST_TXN_MISCINC,AbstractTxn.TRANSFER_TYPE_MISCINCEXP,objLine.getReference()));
					transModel.addLine(new GenerateTransaction(Constants.SPLIT,(Main.mapAccounts.get(objLine.getTicker())),
							objLine.getDate(),0,Main.mapAccounts.get(objLine.getTicker()).getAccountName(),"",AbstractTxn.TRANSFER_TYPE_MISCINCEXP,
							objLine.getReference()));
					transModel.addLine(new GenerateTransaction(Constants.SPLIT,objMatch.getAccount(),objLine.getDate(),
							objLine.getValue()*-1,
							OnlineTxn.INVEST_TXN_MISCINC+" "+Main.mapAccounts.get(objLine.getTicker()).getAccountName(),
							"",AbstractTxn.TRANSFER_TYPE_MISCINCEXP, objLine.getReference()));				
					break;
				case Constants.SECURITY_DIVIDEND :
					transModel.addLine(new GenerateTransaction(Constants.PARENT,acct,objLine.getDate(),
							objLine.getValue(),"",OnlineTxn.INVEST_TXN_DIVIDEND,AbstractTxn.TRANSFER_TYPE_DIVIDEND,objLine.getReference()));
					transModel.addLine(new GenerateTransaction(Constants.SPLIT,(Main.mapAccounts.get(objLine.getTicker())),
							objLine.getDate(),0,Main.mapAccounts.get(objLine.getTicker()).getAccountName(),"",AbstractTxn.TRANSFER_TYPE_DIVIDEND,objLine.getReference()));
					transModel.addLine(new GenerateTransaction(Constants.SPLIT,objMatch.getAccount(),objLine.getDate(),
							objLine.getValue()*-1,
							OnlineTxn.INVEST_TXN_DIVIDEND+" "+Main.mapAccounts.get(objLine.getTicker()).getAccountName(),
							"",AbstractTxn.TRANSFER_TYPE_DIVIDEND,objLine.getReference()));
					
					break;
				case Constants.SECURITY_COST :
					transModel.addLine(new GenerateTransaction(Constants.PARENT,acct,objLine.getDate(),
							objLine.getValue()*-1,"",OnlineTxn.INVEST_TXN_MISCINC,AbstractTxn.TRANSFER_TYPE_MISCINCEXP,objLine.getReference()));
					transModel.addLine(new GenerateTransaction(Constants.SPLIT,(Main.mapAccounts.get(objLine.getTicker())),
							objLine.getDate(),0,Main.mapAccounts.get(objLine.getTicker()).getAccountName(),"",AbstractTxn.TRANSFER_TYPE_MISCINCEXP,
							objLine.getReference()));
					transModel.addLine(new GenerateTransaction(Constants.SPLIT,objMatch.getAccount(),objLine.getDate(),
							objLine.getValue(),
							OnlineTxn.INVEST_TXN_MISCINC+" "+Main.mapAccounts.get(objLine.getTicker()).getAccountName(),
							"",AbstractTxn.TRANSFER_TYPE_MISCINCEXP, objLine.getReference()));				
					break;
				case Constants.INVESTMENT_INCOME:
					transModel.addLine(new GenerateTransaction(Constants.PARENT,acct,objLine.getDate(),
							objLine.getValue(),objLine.getDescription(),"",AbstractTxn.TRANSFER_TYPE_BANK,objLine.getReference()));
					transModel.addLine(new GenerateTransaction(Constants.SPLIT,objMatch.getAccount(),objLine.getDate(),
							objLine.getValue()*-1,acct.getAccountName(),"",AbstractTxn.TRANSFER_TYPE_BANK,objLine.getReference()));
					break;
				case Constants.INVESTMENT_COST :
					transModel.addLine(new GenerateTransaction(Constants.PARENT,acct,objLine.getDate(),
							objLine.getValue(),objLine.getDescription(),"",AbstractTxn.TRANSFER_TYPE_BANK,objLine.getReference()));
					transModel.addLine(new GenerateTransaction(Constants.SPLIT,objMatch.getAccount(),objLine.getDate(),
							objLine.getValue()*-1,acct.getAccountName(),"",AbstractTxn.TRANSFER_TYPE_BANK,objLine.getReference()));
					break;
				default :
					/*
					 * not interested in buys/sells/transfers/card payments
					 */
				}
			}
		}
		
		
	}
	 
	 public void close() {
		this.setVisible(false);
		JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
		topFrame.dispose();

	 }
	 
	 public void save() {
		 int i = 0;
		 while (i<transModel.getRowCount()) {
			 if (transModel.getLine(i).getType() == Constants.PARENT) {
				 if ((boolean)transModel.getValueAt(i, 0)) {
					 GenerateTransaction transLine = transModel.getLine(i);					 
					 switch (transLine.getTType()){
					 case AbstractTxn.TRANSFER_TYPE_MISCINCEXP :
					 case AbstractTxn.TRANSFER_TYPE_DIVIDEND :
						 GenerateTransaction objLine2 = transModel.getLine(i+1);
						 GenerateTransaction objLine3 = transModel.getLine(i+2);
						 ParentTxn ptTran = new ParentTxn(Main.acctBook);
						 ptTran.setDateInt(transLine.getDate());
						 ptTran.setTaxDateInt(transLine.getDate());
						 ptTran.setCheckNumber(transLine.getCheque());
						 ptTran.setAccount(transLine.getAccount());
						 ptTran.setDescription(transLine.getDesc());
						 ptTran.setMemo("");
						 ptTran.setTransferType(transLine.getTType());
						 ptTran.setParameter(Constants.TAGGEN, transLine.getRef());
						 
						 /*
						  * Amount needs to be negative as SplitTxn will negate parent amount						  * 
						  */
						 SplitTxn stTran1 = new SplitTxn(ptTran);
						 stTran1.setCheckNumber(objLine2.getCheque());
						 stTran1.setAccount(objLine2.getAccount());
						 stTran1.setDescription(objLine2.getDesc());
						 stTran1.setAmount(0);
						 stTran1.setParameter(Constants.TAGGEN, objLine2.getRef());
						 stTran1.setParameter(AbstractTxn.TAG_INVST_SPLIT_TYPE, AbstractTxn.TAG_INVST_SPLIT_SEC);
						 ptTran.addSplit(stTran1);
						 SplitTxn stTran2 = new SplitTxn(ptTran);
						 stTran2.setCheckNumber(objLine3.getCheque());
						 stTran2.setAmount(objLine3.getAmount(),1.0D,transLine.getAmount());
						 stTran2.setAccount(objLine3.getAccount());
						 stTran2.setDescription(objLine3.getDesc());
						 stTran2.setParameter(Constants.TAGGEN, objLine3.getRef());
						 stTran2.setParameter(AbstractTxn.TAG_INVST_SPLIT_TYPE, AbstractTxn.TAG_INVST_SPLIT_INC);
						 ptTran.addSplit(stTran2);
						 transLine.setIndex(i);
						 objLine2.setIndex(i+1);
						 objLine3.setIndex(i+1);
						 i+=3;
						 ptTran.syncItem();
						 break;
					 case AbstractTxn.TRANSFER_TYPE_BANK :
						 GenerateTransaction objLineTB = transModel.getLine(i);
						 GenerateTransaction objLineTB2 = transModel.getLine(i+1);
						 ParentTxn ptTranTB = new ParentTxn(Main.acctBook);
						 ptTranTB.setDateInt(objLineTB.getDate());
						 ptTranTB.setTaxDateInt(objLineTB.getDate());
						 ptTranTB.setCheckNumber(objLineTB.getCheque());
						 ptTranTB.setAccount(objLineTB.getAccount());
						 ptTranTB.setDescription(objLineTB.getDesc());
						 ptTranTB.setMemo("");
						 ptTranTB.setTransferType(objLineTB.getTType());
						 ptTranTB.setParameter(Constants.TAGGEN, objLineTB.getRef());
						 SplitTxn stTranTB1 = new SplitTxn(ptTranTB);
						 stTranTB1.setCheckNumber(objLineTB2.getCheque());
						 stTranTB1.setAccount(objLineTB2.getAccount());
						 stTranTB1.setAmount(objLineTB2.getAmount(),1.0D,objLineTB.getAmount());
						 stTranTB1.setDescription(objLineTB2.getDesc());
						 stTranTB1.setParameter(Constants.TAGGEN, objLineTB2.getRef());
						 stTranTB1.setParameter(AbstractTxn.TAG_INVST_SPLIT_TYPE, AbstractTxn.TAG_INVST_SPLIT_SEC);
						 ptTranTB.addSplit(stTranTB1);
						 objLineTB.setIndex(i);
						 objLineTB2.setIndex(i+1);
						 i+=2;
						 ptTranTB.syncItem();
						 break;
					 }				 
				 }
				 else
					 i++;
			 }
			 else
				 i++;
		 }
		 /*
		  * finished creating transactions:
		  *   1. check processed transactions again
		  *   2. Delete processed rows from window
		  *   3. redisplay window 
		  */
		 Main.tsTrans = Main.acctBook.getTransactionSet();
		 Main.mtsGeneratedTrans = new MyTransactionSet(Main.root, acct,objParms,setLine);
		 for (SecLine objLine :setLine) {
			if (!objLine.getSelect())
				continue;
			objLine.setProcessed(false);
			Main.mtsGeneratedTrans.findTransaction(objLine);
		 }
		 int iRowCount = transModel.getRowCount();
		 for (int j = iRowCount -1; j>= 0; j--) {
			 int iIndex = transModel.getLine(j).getIndex();
			 if (iIndex >= 0)
				 transModel.deleteLine(iIndex);
		 }
		 transModel.fireTableDataChanged();

	 }

}
