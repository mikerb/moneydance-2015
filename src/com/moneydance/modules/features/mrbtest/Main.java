/*
  * Copyright (c) 2014, Michael Bray. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - The name of the author may not used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.moneydance.modules.features.mrbtest;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

import com.infinitekind.util.CustomDateFormat;
import com.moneydance.apps.md.controller.FeatureModule;
import com.moneydance.apps.md.controller.FeatureModuleContext;
import com.moneydance.apps.md.controller.UserPreferences;
import com.moneydance.awt.GridC;
import com.moneydance.awt.JDateField;
import com.moneydance.modules.features.jasperreports.TemplateRow;
import com.moneydance.modules.features.mrbutil.MRBDebug;
import com.moneydance.modules.features.mrbutil.MRBPreferences;

/**
 * MoneyDance extension to generate budget items for the 'new' type of budgets
 * 
 * Main class to create main window
 */

public class Main extends FeatureModule {
	public static CustomDateFormat cdate;
	public static FeatureModuleContext context;
	public UserPreferences up = null;
	public static Image imgIcon;
	public static String strBuild;
	private JFrame frame = new JFrame();
	private boolean closeRequested;
	private JPanel screenPan;
	MRBPreferences preferences;
	JDateField date;
	JDateField curdate;
	String PROGRAMNAME = "securityquoteload";
	String SECLASTRUN = "lastrun";
	String CURLASTRUN = "curlastrun";




	@Override
	public void init() {
		// the first thing we will do is register this module to be invoked
		// via the application toolbar
		context = getContext();
		try {
			imgIcon = getIcon("/com/moneydance/modules/features/mrbtest/mrb icon2.png");
			context.registerFeature(this, "showconsole",
					imgIcon, getName());
		} catch (Exception e) {
			e.printStackTrace(System.err);
		}
	    int iBuild = getBuild();
	    strBuild = String.valueOf(iBuild);  

	}

	/*
	 * Get Icon is not really needed as Icons are not used. Included as the
	 * register feature method requires it
	 */

	private Image getIcon(String action) {
		try {
			ClassLoader cl = getClass().getClassLoader();
			java.io.InputStream in = cl
					.getResourceAsStream(action);
			if (in != null) {
				ByteArrayOutputStream bout = new ByteArrayOutputStream(1000);
				byte buf[] = new byte[256];
				int n = 0;
				while ((n = in.read(buf, 0, buf.length)) >= 0)
					bout.write(buf, 0, n);
				return Toolkit.getDefaultToolkit().createImage(
						bout.toByteArray());
			}
		} catch (Throwable e) {
		}
		return null;
	}
	/*
	 * This does not seem to be called, make sure handleEvent("md:file:closing") is present
	 */
	@Override
	public void cleanup() {
		closeConsole();
	}

	/*
	 * determine if file is being closed and close down extension
	 */
	@Override
	public void handleEvent(String appEvent) {

		if ("md:file:closing".equals(appEvent)) {
			closeConsole();
		}
	}

	/** Process an invocation of this module with the given URI */
	@Override
	public void invoke(String uri) {
		String command = uri;
		String strDateFormat;
		up = UserPreferences.getInstance();
		strDateFormat = up.getSetting(UserPreferences.DATE_FORMAT);
		cdate = new CustomDateFormat(strDateFormat);
		int theIdx = uri.indexOf('?');
		if (theIdx >= 0) {
			command = uri.substring(0, theIdx);
		} else {
			theIdx = uri.indexOf(':');
			if (theIdx >= 0) {
				command = uri.substring(0, theIdx);
			}
		}
		MRBPreferences.loadPreferences(context);
		preferences = MRBPreferences.getInstance();

		if (command.equals("showconsole")) {
			showConsole();
		}
	}

	@Override
	public String getName() {
		return "Test Bed";
	}

	private void createAndShowGUI() {
		closeRequested = false;
			frame = new JFrame();
			frame.add(new panel());
//		catch (Exception e) {
//			e.printStackTrace();
//		}
		if (!closeRequested) {
			frame.setTitle("MoneyDance test");
			frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
			//Display the window.
			frame.pack();
			frame.setLocationRelativeTo(null);
			frame.addWindowListener(new java.awt.event.WindowAdapter() {
			    @Override
			    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
			        if (JOptionPane.showConfirmDialog(frame, 
			            "Are you sure you want to close Jasper Reports?", "Close Window?", 
			            JOptionPane.YES_NO_OPTION,
			            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
			            closeConsole();
			        }
			    }
			});
			frame.setVisible(true);
		}

	}
	/**
	 * Starts the user interface for the extension
	 * 
	 * First it checks if Rhumba is present by sending a hello message to Rhumba
	 * @see #invoke(String)
	 */
	private synchronized void showConsole() {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				createAndShowGUI();
			}
			});

	}

	FeatureModuleContext getUnprotectedContext() {
		return getContext();
	}

	synchronized void closeConsole() {
			frame.dispose();
			System.gc();
		
	}
	private void updateDate(String node,JDateField date){
		preferences.put(PROGRAMNAME+"."+node,date.getDateInt());
		preferences.isDirty();
	}
	private void resetDate(String node,JDateField date){
		preferences.put(PROGRAMNAME+"."+node,(String)null);
		preferences.isDirty();
		date.setDateInt(0);
	}
	public class panel extends JPanel {
		JPanel pane1;
		JPanel pane2;
		JPanel pane3;
		JPanel pane4;
		JPanel pane11;
		JPanel pane12;
		JPanel pane13;
		JPanel pane14;
		public panel() {
			super();
			this.addComponentListener(new ComponentListener() {

				@Override
				public void componentResized(ComponentEvent arg0) {
					JPanel panScreen = (JPanel) arg0.getSource();
					Dimension dimension = panScreen.getSize();
					setSizes(dimension);
					validate();
				}

				@Override
				public void componentShown(ComponentEvent arg0) {
					// not needed
				}

				@Override
				public void componentHidden(ComponentEvent e) {
					// not needed
				}

				@Override
				public void componentMoved(ComponentEvent e) {
					// not needed
				}

			});		
			GridBagLayout layout = new GridBagLayout();
			setLayout(layout);
			TemplateTableModel model = new TemplateTableModel();
			TemplateTable table = new TemplateTable(model);
			pane1 = new JPanel(new BorderLayout());
			JScrollPane sppane = new JScrollPane(table);
			pane1.setMinimumSize(new Dimension(100,100));
			pane1.add(sppane,BorderLayout.CENTER);
			add(pane1, GridC.getc(0,0).wxy(1.0f, 1.0f));
			pane2 = new JPanel(new BorderLayout());
			JButton button = new JButton("+");
			button.setPreferredSize(new Dimension(100,100));
			pane2.add(button,BorderLayout.CENTER);
			add(pane2, GridC.getc(1,0).wxy(1.0f, 1.0f));
			TemplateTable table2 = new TemplateTable(model);
			pane3 = new JPanel(new BorderLayout());
			JScrollPane sppane2 = new JScrollPane(table2);
			pane3.setMinimumSize(new Dimension(100,100));
			pane3.add(sppane2,BorderLayout.CENTER);
			add(pane3, GridC.getc(2,0).wxy(1.0f, 1.0f));
			pane4 = new JPanel(new BorderLayout());
			JButton button2 = new JButton("+");
			button2.setPreferredSize(new Dimension(100,100));
			pane4.add(button2,BorderLayout.CENTER);
			add(pane4, GridC.getc(3,0).wxy(1.0f, 1.0f));
			TemplateTableModel model1 = new TemplateTableModel();
			TemplateTable table1 = new TemplateTable(model1);
			pane11 = new JPanel(new BorderLayout());
			JScrollPane sppane1 = new JScrollPane(table1);
			pane11.setMinimumSize(new Dimension(100,100));
			pane11.add(sppane1,BorderLayout.CENTER);
			add(pane11, GridC.getc(0,1).wxy(1.0f, 1.0f));
			pane12 = new JPanel(new BorderLayout());
			JButton button1 = new JButton("+");
			button1.setPreferredSize(new Dimension(100,100));
			pane12.add(button1,BorderLayout.CENTER);
			add(pane12, GridC.getc(1,1).wxy(1.0f, 1.0f));
			TemplateTable table12 = new TemplateTable(model1);
			pane13 = new JPanel(new BorderLayout());
			JScrollPane sppane12 = new JScrollPane(table12);
			pane13.setMinimumSize(new Dimension(100,100));
			pane13.add(sppane12,BorderLayout.CENTER);
			add(pane13, GridC.getc(2,1).wxy(1.0f, 1.0f));
			pane14 = new JPanel(new BorderLayout());
			JButton button12 = new JButton("+");
			button12.setPreferredSize(new Dimension(100,100));
			pane14.add(button12,BorderLayout.CENTER);
			add(pane14, GridC.getc(3,1).wxy(1.0f, 1.0f));
			
		}
		private void setSizes(Dimension dim) {
			int spwidth = dim.width * 4/10;
			int btwidth = dim.width/10;
			pane1.setMinimumSize(new Dimension(spwidth,dim.height/2));
			pane3.setMinimumSize(new Dimension(spwidth,dim.height/2));
			pane11.setMinimumSize(new Dimension(spwidth,dim.height/2));
			pane13.setMinimumSize(new Dimension(spwidth,dim.height/2));
		}
		
	}
	protected class TemplateTable extends JTable{
		private TemplateTableModel model;
		private JTableHeader header;
		private MRBDebug debugInst = MRBDebug.getInstance();
		public boolean isColumnWidthChanged;
		public  int nameCol = 0;
		public  int lastverifiedCol = 1;
	    private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	    private Double screenHeight;
	    private Double screenWidth;

		public TemplateTable (TemplateTableModel modelp) {
			super(modelp);
			model=modelp;
			screenHeight = screenSize.getHeight();
			screenWidth = screenSize.getWidth();
			header = getTableHeader();
			setRowHeight(20);
//			this.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
//			this.setPreferredScrollableViewportSize(this.getPreferredSize());
//			this.setFillsViewportHeight(true);
			this.setPreferredSize(new Dimension(500,500));
			this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			this.setCellSelectionEnabled(true);
			this.addMouseListener(new TableMouseListener());
			/*
			 * Name
			 */
			TableColumn colSelect = this.getColumnModel().getColumn(nameCol);
			/*
			 * Ticker
			 */
			this.getColumnModel().getColumn(lastverifiedCol).setResizable(true);

		}
    	public boolean getColumnWidthChanged() {
		        return isColumnWidthChanged;
		}

		public void setColumnWidthChanged(boolean widthChanged) {
		        isColumnWidthChanged = widthChanged;
		}
		private class TableMouseListener extends MouseAdapter
		{
			@Override
		    public void mouseReleased(MouseEvent e)
		    {
				JTable tc = (JTable)e.getSource();
	       		Point p = e.getPoint();
	       		int row = tc.rowAtPoint(p);
	  	       	if (SwingUtilities.isRightMouseButton(e) || e.isControlDown() ) {
	  	       		displayTemplateMenu(row);
	  	       	}
		    }
		}
		private void displayTemplateMenu(int row){
		    debugInst.debug("TemplateTable", "displayTemplateMenu", MRBDebug.SUMMARY, "on row "+row );    
			TemplateRow template = model.getRow(row);
			Rectangle rect = this.getCellRect(row, lastverifiedCol, false);
			ActionListener tickerListener = new ActionListener () {
				@Override
				public void actionPerformed(ActionEvent aeEvent) {
					String strAction = aeEvent.getActionCommand();
				}
			};		
			JPopupMenu menu=new JPopupMenu();
			JMenuItem test = new JMenuItem("Verify template "+template.getName());
			test.addActionListener(tickerListener);
			menu.add(test);
			menu.show(this, rect.x+rect.width, rect.y);
		}
		private void VerifyTemplate(TemplateRow template) {
	
		}
	}
	protected class TemplateTableModel extends DefaultTableModel{
		private List<TemplateRow> templates;
		private String[] arrColumns = {"Name","Last Verified Date"};
		public TemplateTableModel () {
			super();
			templates =new ArrayList<>();
			TemplateRow row = new TemplateRow();
		}
		@Override
		public int getRowCount() {
			if (templates == null)
				return 0;
			return templates.size();
		}
		@Override
		public Class getColumnClass(int c){
			return String.class;
		}

		@Override
		public int getColumnCount() {
				return arrColumns.length;
		}	
		@Override
		public String getColumnName(int c) {
			return arrColumns[c];
		}
		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			switch (columnIndex) {
			/*
			 * Select
			 */
			case 0:
				return  templates.get(rowIndex).getName();
			/*
			 * Ticker
			 */
			case 1:
				return  templates.get(rowIndex).getLastVerified();
			default :
				return " ";
			}
		}
		@Override
	    public boolean isCellEditable(int row, int col) {
				return false;
	    }
		public TemplateRow getRow(int row) {
			return templates.get(row);
		}
	}
}
