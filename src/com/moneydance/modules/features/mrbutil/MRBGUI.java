package com.moneydance.modules.features.mrbutil;

import java.awt.Image;

import javax.swing.Icon;

import com.moneydance.apps.md.controller.UserPreferences;
import com.moneydance.apps.md.view.resources.MDResourceProvider;
import com.moneydance.apps.md.view.resources.Resources;

public class MRBGUI implements MDResourceProvider {
	private Resources objResources;
	private UserPreferences objUserPref;

	public MRBGUI () {
		objUserPref = UserPreferences.getInstance();
		objResources = objUserPref.getResources();
	}
	public UserPreferences getPreferences(){
		return objUserPref;
	}
	@Override
	public Icon getIcon(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Image getImage(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Resources getResources() {
		return objResources;
	}

	@Override
	public String getStr(String strKey) {
		return objResources.getString(strKey);
	}


}
