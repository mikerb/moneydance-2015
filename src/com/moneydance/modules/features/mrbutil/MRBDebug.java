package com.moneydance.modules.features.mrbutil;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class MRBDebug {
	private int iLevel = 0;
	public static final int  OFF = 0;
	public static final int INFO = 1;
	public static final int SUMMARY = 2;
	 public static int DETAILED = 3;
	private static MRBDebug objDebug = null;
	private static DateTimeFormatter dtf =DateTimeFormatter.ofPattern("HH:mm:ss"); 
	private String strExtension = "";
	public MRBDebug() {
		objDebug = this;
	}
	public static MRBDebug getInstance() {
		if (objDebug == null)
			objDebug = new MRBDebug();
		return objDebug;
			
	}
	public int getDebugLevel () {
		return iLevel;
	}
	public void setDebugLevel (int iLevelp) {
		iLevel = iLevelp;
	}
	public void setExtension (String strExtensionp) {
		strExtension = strExtensionp;
	}
	public void debugThread(String strClass, String strMethod,int iLevelp, String strMessage) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				debug(strClass, strMethod,iLevelp, strMessage);
			}
			});
  
	}
	public void debug (String strClass, String strMethod,int iLevelp, String strMessage) {
		LocalTime now= LocalTime.now();

		if (iLevel != OFF && iLevelp <= iLevel) {
			String type="";
			switch (iLevelp) {
			case INFO:
				type = "INFO";
				break;
			case SUMMARY:
				type = "SUMM";
				break;
			default :
				type = "DET";
			}
			System.err.println(strExtension + ">"+type+":"+now.format(dtf)+"-"+Thread.currentThread().getName()+"("+strClass+","+strMethod+ ") " +strMessage);
		}
	}
	
}
