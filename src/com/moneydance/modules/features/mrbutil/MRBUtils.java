package com.moneydance.modules.features.mrbutil;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

public class MRBUtils {
	public static final int countFields(String strTarget, char chDelimiter) {
		if (strTarget == null)
			return 0;
		int iCount = 0;
		int iIndex = -1;
		do {
			iCount++;
			iIndex = strTarget.indexOf(chDelimiter, iIndex + 1);
		} while (iIndex >= 0);
		return iCount;
	}

	public static final String fieldIndex(String strTarget, char chDelimiter,int iIndex) {
		int iCrntField = 0;
		int iLastIx = 0;
		int iTargetLen = strTarget.length();
		do {
			if (iCrntField == iIndex) {
				int iThisTemp = strTarget.indexOf(chDelimiter, iLastIx);
				if (iThisTemp >= 0) {
					return strTarget.substring(iLastIx, iThisTemp);
				}
				return strTarget.substring(iLastIx, iTargetLen);
			}
			int iThisIndex = strTarget.indexOf(chDelimiter, iLastIx);
			if (iThisIndex < 0) {
				return "";
			}
			iCrntField++;
			iLastIx = iThisIndex + 1;
		}
		while (iLastIx < iTargetLen);
		return "";
	}
	public static javafx.scene.image.Image createImage(java.awt.Image image) throws IOException {
		if (image == null)
			return null;
	    if (!(image instanceof RenderedImage)) {
	      BufferedImage bufferedImage = new BufferedImage(image.getWidth(null),
	              image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
	      Graphics g = bufferedImage.createGraphics();
	      g.drawImage(image, 0, 0, null);
	      g.dispose();

	      image = bufferedImage;
	    }
	    ByteArrayOutputStream out = new ByteArrayOutputStream();
	    ImageIO.write((RenderedImage) image, "png", out);
	    out.flush();
	    ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
	    return new javafx.scene.image.Image(in);
	  }
}
