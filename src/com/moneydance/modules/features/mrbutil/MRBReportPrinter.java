package com.moneydance.modules.features.mrbutil;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.moneydance.apps.md.controller.UserPreferences;
import com.moneydance.util.StringUtils;
/**
 * Prints a report.
 * <p>
 * The number of pages required is calculated. The user is asked to select the required number
 *  of pages wide is required.  The report is scaled accordingly
 *  
 * @author Mike Bray
 *
 */
public class MRBReportPrinter implements MRBPrintable {
	private UserPreferences objPrefs;
	private MRBReport objReport;
	private MRBReportFonts objFonts = null;
	private MRBReportViewer objViewer;
	private double dNormalHeaderHeight = 0.0D;
	private int iNumLinesTitlePage = 5;
	private int iNumLinesNormalPage = 5;
	private int iNumPages = 1;
	private int iLineHeight = 10;
	private int[] arriColumnWidths = null; // calculated widths
	private int[] arrPageColumns = null; // start column for each page
	private double[] arrdColumnRelativeWidths = null; // original widths from
														// Viewer
	private int COLUMN_SPACING = 1;
	private Color clrColor1 = new Color(255, 255, 255);
	private Color clrColor2 = new Color(245, 245, 245);

	private boolean bInfoCalculated = false;
	private boolean bPagesCalculated = false;
	private int iHeaderRows = 1;
	private int iPagesWide; // number of horizontal pages

	private double dScale = 1.0D;
	private double dTotalColumnWidth;
	/**
	 * Creates a report printer
	 * 
	 * @param objViewerp - the MRBReportViewer that is displaying the report to be printed
	 * @param arrdColWidths - the column widths
	 */
	public MRBReportPrinter(MRBReportViewer objViewerp, double[] arrdColWidths) {
		objPrefs = UserPreferences.getInstance();
		objViewer = objViewerp;
		objReport = objViewer.getReport();
		arrdColumnRelativeWidths = arrdColWidths;
		objFonts = MRBReportFonts.getPrintingFonts(objPrefs);
	}
	@Override
	public String getTitle() {
		return objReport.getTitle();
	}

	@Override
	public boolean usesWholePage() {
		return false;
	}


	@Override
	public boolean printPage(Graphics objGraphics, int iCrntPage,
			double dWidth, double dHeight, int iResolution) {
		calculatePages(dWidth, dHeight);
		int iRealPage;
		int iHorizontalPage;
		if ((dWidth <= 0.0D) || (dHeight <= 0.0D))
			return false;

		int iLeftX = 0;
		if (iPagesWide > 1) {
			/*
			 * report will be more than one page wide determine actual page for
			 * printing purposes
			 */
			iRealPage = iCrntPage / (iPagesWide);
		} else
			iRealPage = iCrntPage;
		iHorizontalPage = iCrntPage - (iRealPage * iPagesWide)+1;
		/*
		 * set up 2 Dimension Graphics
		 */
		Graphics2D obj2DGraphics = (Graphics2D) objGraphics;

		calculateInfo(obj2DGraphics, dHeight, dWidth);

		int iImageWidth = (int) Math.floor(dWidth / dScale);
		int iImageHeight = (int) Math.floor(dHeight / dScale);

		int iCrntY = 0;
		obj2DGraphics.setColor(Color.black);

		int iCrntLine = 0;

		int iReportRowCount = objReport.getRowCount();
		int iNumCols = objReport.getColumnCount();
		int iLineCrntPage;
		if (iRealPage <= 0) {
			iLineCrntPage = iNumLinesTitlePage;
		} else {
			iCrntLine += iNumLinesTitlePage;
			int iPageCount = iRealPage;
			while (iPageCount > 1) {
				iCrntLine += iNumLinesNormalPage;
				iPageCount--;
			}
			if (iCrntLine >= iReportRowCount) 
				iLineCrntPage = iNumLinesNormalPage - iCrntLine+iReportRowCount;
			else
				iLineCrntPage = iNumLinesNormalPage;
		}

		setOutputScale(obj2DGraphics, (int) Math.ceil(dWidth));
		if (iRealPage <= 0) {
			iCrntY = drawTitleAndSubtitle(obj2DGraphics, iLeftX, iCrntY,
					iImageWidth);
		}

		iCrntY = drawReportColumnHeaders(obj2DGraphics, iLeftX, iCrntY,
				iImageWidth, iCrntPage - iRealPage * (iPagesWide));

		iCrntLine = drawReportRows(obj2DGraphics, iLeftX, iCrntY, iImageWidth,
				iCrntLine, iLineCrntPage, iReportRowCount, iNumCols, iCrntPage - iRealPage
						* (iPagesWide));

		drawPageFooterText(obj2DGraphics, iLeftX, iImageWidth, iImageHeight,
				iRealPage, iCrntPage - iRealPage * (iPagesWide));

		/*
		 * if all rows have been dealt with and the last horizontal page has been printed
		 * return false to indicate end of report.
		 */
		if (iCrntLine >= objReport.getRowCount()
				&& (iHorizontalPage >= iPagesWide))
			return false;
		return true;
	}

	private void setOutputScale(Graphics2D obj2DGraphics, int iPageWidth) {
		if (dScale != 1.0D) {
			obj2DGraphics.scale(dScale, dScale);
		} else {
			/*
			 * Move origin so text is in centre of page
			 */
			int iDiff = (int) Math
					.floor((iPageWidth - dTotalColumnWidth) / 2.0D);
			obj2DGraphics.translate(iDiff, 0);
		}
	}

	private int drawTitleAndSubtitle(Graphics2D g, int leftX, int currentY,
			double width) {
		g.setFont(objFonts.getTitleFont());
		FontMetrics fm = g.getFontMetrics();
		currentY += fm.getHeight();
		String title = objReport.getTitle();
		if ((title != null) && (title.length() > 0)) {
			g.drawString(title,
					leftX + (int) (width / 2.0D - fm.stringWidth(title) / 2),
					currentY - fm.getMaxDescent() - 1);
		}
		String date = objReport.getSubTitle();
		if ((date != null) && (date.length() > 0)) {
			g.setFont(objFonts.getSubtitleFont());
			fm = g.getFontMetrics();
			currentY += fm.getHeight();
			g.drawString(date,
					leftX + (int) (width / 2.0D - fm.stringWidth(date) / 2),
					currentY - fm.getMaxDescent() - 1);
		}
		return currentY;
	}

	private int drawReportColumnHeaders(Graphics2D g, int leftX, int currentY,
			double width, int iPage) {
		g.setFont(objFonts.getHeaderFont());
		FontMetrics fm = g.getFontMetrics();
		int rowHeight = fm.getHeight();
		currentY = (int) (currentY + dNormalHeaderHeight);
		int headerHeight = (int) dNormalHeaderHeight;
		g.setClip(leftX, currentY - headerHeight, (int) width, headerHeight);
		g.setColor(Color.lightGray);
		g.drawLine(leftX, currentY - 2, leftX + (int) width, currentY - 2);

		int headerY = currentY;
		for (int row = iHeaderRows - 1; row >= 0; row--) {
			int currentX = leftX;
			int iLastColumn;
			int iFirstColumn = arrPageColumns[iPage];
			if (iPage < iPagesWide-1)
				iLastColumn = arrPageColumns[iPage + 1];
			else
				iLastColumn = objReport.getColumnCount();
			/*
			 * if not first horizontal page draw row headers
			 */
			if (iFirstColumn > 0) {
				for (int ii = 0; ii < objReport.getRowHeaders(); ii++) {
					String columnName = objReport.getColumnNameNoHTML(ii);
					String[] lines = columnName.split("\n");

					g.setClip(currentX, headerY - rowHeight,
							arriColumnWidths[ii], rowHeight);
					if (ii > 0) {
						g.setColor(Color.lightGray);
						g.drawLine(currentX + 1, currentY - headerHeight,
								currentX + 1, currentY);
					}

					if (row < lines.length) {
						if (!StringUtils.isBlank(lines[row])) {
							g.setColor(Color.black);
							g.drawString(
									lines[row],
									currentX + arriColumnWidths[ii] / 2
											- fm.stringWidth(lines[row]) / 2,
									headerY - fm.getMaxDescent() - 1);
						}
					}
					currentX += arriColumnWidths[ii] + COLUMN_SPACING;
				}
			}
			/*
			 * Draw columns for this page
			 */
			for (int ii = iFirstColumn; ii < iLastColumn; ii++) {
				String columnName = objReport.getColumnNameNoHTML(ii);
				String[] lines = columnName.split("\n");

				g.setClip(currentX, headerY - rowHeight, arriColumnWidths[ii],
						rowHeight);
				if (ii > 0) {
					g.setColor(Color.lightGray);
					g.drawLine(currentX + 1, currentY - headerHeight,
							currentX + 1, currentY);
				}

				if (row < lines.length) {
					if (!StringUtils.isBlank(lines[row])) {
						g.setColor(Color.black);
						g.drawString(
								lines[row],
								currentX + arriColumnWidths[ii] / 2
										- fm.stringWidth(lines[row]) / 2,
								headerY - fm.getMaxDescent() - 1);
					}
				}
				currentX += arriColumnWidths[ii] + COLUMN_SPACING;
			}
			headerY -= rowHeight;
		}
		return currentY;
	}

	private int drawReportRows(Graphics2D g, int leftX, int startY,
			double dPageWidth, int iStartLineNum, int iTotalLinesPage, int iTotalRowsReport,
			int iNumCols, int iPage) {
		int currentY = startY;
		int lineOnPage = 0;
		g.setFont(objFonts.getNormalFont());
		FontMetrics fm = g.getFontMetrics();
		int descent = fm.getMaxDescent() + 1;
		/*
		 * determine first and last columns on page
		 */
		int iLastColumn;
		int iFirstColumn = arrPageColumns[iPage];
		if (iPage < iPagesWide-1)
			iLastColumn = arrPageColumns[iPage + 1];
		else
			iLastColumn = iNumCols;
		/*
		 * loop through and draw each line
		 */
		while ((lineOnPage < iTotalLinesPage) && (iStartLineNum < iTotalRowsReport)) {
			currentY += iLineHeight;
			int currentX = leftX;
			MRBRecordRow row = objReport.getRow(iStartLineNum);
			/*
			 * 
			 */
			if (row != null) {
				g.setClip(currentX, currentY - iLineHeight, (int) dPageWidth,
						iLineHeight);
				g.setColor(lineOnPage % 2 == 0 ? clrColor1 : clrColor2);
				g.fillRect(currentX, currentY - iLineHeight, (int) dPageWidth,
						iLineHeight);
				/*
				 * if not first horizontal page draw row headers
				 */
				if (iFirstColumn > 0) {
					for (int c = 0; c < objReport.getRowHeaders(); c++) {
						int cField = c;
						String value = row.getLabel(cField);
						g.setClip(currentX, currentY - iLineHeight,
								arriColumnWidths[c], iLineHeight);
						if ((value != null) && (value.length() > 0)) {
							if (row.getBorder(cField) == MRBReportViewer.BORDER_TOP) {
								g.setColor(Color.gray);
								g.drawLine(currentX,
										currentY - iLineHeight + 1, currentX
												+ arriColumnWidths[c], currentY
												- iLineHeight + 1);
							} else if (row.getBorder(cField) == MRBReportViewer.BORDER_BOTTOM) {
								g.setColor(Color.black);
								g.drawLine(currentX,
										currentY - iLineHeight + 1, currentX
												+ arriColumnWidths[c], currentY
												- iLineHeight + 1);
							}

							switch (row.getStyle(cField)) {
							case MRBReportViewer.STYLE_BOLD:
								g.setFont(objFonts.getBoldFont());
								break;
							case MRBReportViewer.STYLE_ITALIC:
								g.setFont(objFonts.getItalicFont());
								break;
							case 1:
							case 3:
							default:
								g.setFont(objFonts.getNormalFont());
							}

							g.setColor(row.getColorFG(cField));
							fm = g.getFontMetrics();
							switch (row.getAlignment(cField)) {
							case MRBReportViewer.ALIGN_RIGHT:
								g.drawString(
										value,
										currentX + arriColumnWidths[c]
												- fm.stringWidth(value) - 4,
										currentY - descent);
								break;
							case MRBReportViewer.ALIGN_CENTER:
								g.drawString(
										value,
										currentX
												+ (arriColumnWidths[c] - fm
														.stringWidth(value))
												/ 2, currentY - descent);
								break;
							default:
								g.drawString(value, currentX, currentY
										- descent);
							}
						}

						g.setColor(Color.black);
						currentX += arriColumnWidths[c] + COLUMN_SPACING;
					}
				}
				/*
				 * Draw columns for this page
				 */
				for (int c = iFirstColumn; c < iLastColumn; c++) {
					int cField = c;
					String value = row.getLabel(cField);
					g.setClip(currentX, currentY - iLineHeight,
							arriColumnWidths[c], iLineHeight);
					if ((value != null) && (value.length() > 0)) {
						if (row.getBorder(cField) == MRBReportViewer.BORDER_TOP) {
							g.setColor(Color.gray);
							g.drawLine(currentX, currentY - iLineHeight + 1,
									currentX + arriColumnWidths[c], currentY
											- iLineHeight + 1);
						} else if (row.getBorder(cField) == MRBReportViewer.BORDER_BOTTOM) {
							g.setColor(Color.black);
							g.drawLine(currentX, currentY - iLineHeight + 1,
									currentX + arriColumnWidths[c], currentY
											- iLineHeight + 1);
						}

						switch (row.getStyle(cField)) {
						case MRBReportViewer.STYLE_BOLD:
							g.setFont(objFonts.getBoldFont());
							break;
						case MRBReportViewer.STYLE_ITALIC:
							g.setFont(objFonts.getItalicFont());
							break;
						case 1:
						case 3:
						default:
							g.setFont(objFonts.getNormalFont());
						}

						g.setColor(row.getColorFG(cField));
						fm = g.getFontMetrics();
						switch (row.getAlignment(cField)) {
						case MRBReportViewer.ALIGN_RIGHT:
							g.drawString(value, currentX + arriColumnWidths[c]
									- fm.stringWidth(value) - 4, currentY
									- descent);
							break;
						case MRBReportViewer.ALIGN_CENTER:
							g.drawString(
									value,
									currentX
											+ (arriColumnWidths[c] - fm
													.stringWidth(value)) / 2,
									currentY - descent);
							break;
						default:
							g.drawString(value, currentX, currentY - descent);
						}
					}

					g.setColor(Color.black);
					currentX += arriColumnWidths[c] + COLUMN_SPACING;
				}
				iStartLineNum++;
				lineOnPage++;
			}
		}
		return iStartLineNum;
	}

	private void drawPageFooterText(Graphics2D g, int leftX, double width,
			double height, int pageIndex, int iHorPage) {
		String pageStr = objReport.getFooter();
		pageStr = StringUtils.replaceAll(
				pageStr,
				"{pagenum}",
				String.valueOf(pageIndex + 1) + " - "
						+ String.valueOf(iHorPage + 1));
		pageStr = StringUtils.replaceAll(
				pageStr,
				"{numpages}",
				String.valueOf(iNumPages) + " - "
						+ String.valueOf(iPagesWide));
		g.setFont(objFonts.getNormalFont());
		g.setColor(Color.black);
		FontMetrics fm = g.getFontMetrics();
		g.setClip(leftX, (int) (height - fm.getHeight()), (int) width,
				fm.getHeight());
		g.drawString(pageStr, leftX, (int) (height - fm.getMaxDescent()));
	}


	/**
	 * Determine the number of horizontal pages required and then scale
	 * depending on user choice
	 * @param dWidth - width of the page
	 * @param dHeight - height of the page
	 */
	public synchronized void calculatePages(double dWidth, double dHeight) {
		if (bPagesCalculated)
			return;
		double dColumnWidth = 0.0D;
		double dPageWidth = 0.0D;
		double dHeaderWidth = 0.0D;
		arrPageColumns = new int[objReport.getColumnCount()];
		arriColumnWidths = new int[objReport.getColumnCount()];
		int iCurrentPage = 0;
		iPagesWide = 0;
		arrPageColumns[0] = 0;
		/*
		 * determine width of row headers to be used on each page
		 */
		for (int j = 0; j < objReport.getRowHeaders(); j++) {
			dHeaderWidth += ((int) Math.ceil(arrdColumnRelativeWidths[j]));
		}
		/*
		 * Determine number of horizontal pages using no scaling
		 */
		dTotalColumnWidth = 0.0D;
		for (int i = 0; i < arriColumnWidths.length; i++) {
			arriColumnWidths[i] = ((int) Math.ceil(arrdColumnRelativeWidths[i]));
			if ((dPageWidth + arrdColumnRelativeWidths[i]) > dWidth) {
				arrPageColumns[++iCurrentPage] = i;
				dPageWidth = dHeaderWidth;
			}
			dColumnWidth += arrdColumnRelativeWidths[i];
			dPageWidth += arrdColumnRelativeWidths[i];
		}
		dTotalColumnWidth = dColumnWidth;
		iPagesWide = iCurrentPage + 1;
		;

		if (iPagesWide > 1) {
			/*
			 * more than one page required, ask user to select number to print
			 */
			boolean bDone = false;
			while (!bDone) {

				JFrame frmChoice = new JFrame();
				String[] strNumPages = new String[iPagesWide];
				for (int i = 0; i < iPagesWide; i++)
					strNumPages[i] = String.valueOf(i + 1);
				String strChoice = (String) JOptionPane.showInputDialog(
						frmChoice,
						"Your report will be "
								+ String.valueOf(iCurrentPage + 1)
								+ " pages wide \n\n"
								+ "Select the number of pages required",
						"Report Size", JOptionPane.PLAIN_MESSAGE, null,
						strNumPages, strNumPages[iCurrentPage]);

				// If a string was returned, set new pages wide
				if ((strChoice != null) && (strChoice.length() > 0)) {
					iPagesWide = Integer.parseInt(strChoice);
					bDone = true;
				}
			}
			double dNewWidth = ((iPagesWide) * dHeaderWidth + dTotalColumnWidth)
					/ (iPagesWide);
			dTotalColumnWidth = 0.0D; // will be set to max page width
			arrPageColumns[0] = 0;
			iCurrentPage = 0;
			dPageWidth = 0.0D;
			for (int i = 0; i < arriColumnWidths.length; i++) {
				if ((dPageWidth + arrdColumnRelativeWidths[i]) > dNewWidth) {
					arrPageColumns[++iCurrentPage] = i;
					dTotalColumnWidth = Math.max(dTotalColumnWidth,
							dPageWidth);
					dPageWidth = dHeaderWidth;
				}
				dColumnWidth += arrdColumnRelativeWidths[i];
				dPageWidth += arrdColumnRelativeWidths[i];
			}
			dTotalColumnWidth = Math.max(dTotalColumnWidth, dPageWidth);
			iPagesWide = iCurrentPage + 1;
				
		}
		bPagesCalculated = true;
	}

	private synchronized void calculateInfo(Graphics2D obj2DGraphics,
			double dHeight, double dWidth) {
		if (bInfoCalculated)
			return;
		dScale = 1.0D;
		/*
		 * TotalColumnWidth set to maximum page width. All pages scaled to this
		 */
		if (dTotalColumnWidth > dWidth) {
			dScale = (dWidth / dTotalColumnWidth);
		}

		objFonts = MRBReportFonts.getPrintingFonts(objPrefs);

		AffineTransform oldTrans = obj2DGraphics.getTransform(); // save for restore
																	

		setOutputScale(obj2DGraphics, (int) Math.ceil(dWidth));
		int iScaledHeight = (int) Math.floor(dHeight / dScale);

		scaleHeaderFont(obj2DGraphics);

		FontMetrics fmNormal = obj2DGraphics.getFontMetrics(objFonts
				.getNormalFont());
		/*
		 * calculate line height with 4 units of spacing
		 */
		iLineHeight = (fmNormal.getHeight() + 4);

		/*
		 * get Title font height
		 */
		double dTitleHeaderHeight = obj2DGraphics.getFontMetrics(
				objFonts.getTitleFont()).getHeight();
		/*
		 * add subtitle height if present
		 */
		String strSubtitle = objReport.getSubTitle();
		if (!StringUtils.isBlank(strSubtitle)) {
			dTitleHeaderHeight += obj2DGraphics.getFontMetrics(
					objFonts.getSubtitleFont()).getHeight();
		}
		/*
		 * calculate column header height, add 10 units for spacing
		 */
		int iHeaderRowHeight = Math.round(obj2DGraphics.getFontMetrics(
				objFonts.getHeaderFont()).getHeight())+5;
		dTitleHeaderHeight += iHeaderRowHeight+5;
		/*
		 * calculate footer height with 4 units for spacing 
		 */
		
		double dFooterHeight = fmNormal.getHeight() + 4;

		dNormalHeaderHeight = (iHeaderRows * iHeaderRowHeight + 2);

		/*
		 * calculate number of lines for title page and normal pages
		 */
		iNumLinesTitlePage = Math.max(5, (int) Math.floor((iScaledHeight
				- dTitleHeaderHeight - dFooterHeight)
				/ iLineHeight));

		iNumLinesNormalPage = Math.max(5, (int) Math.floor((iScaledHeight
				- dNormalHeaderHeight - dFooterHeight)
				/ iLineHeight));

		/*
		 * calculate number of pages deep
		 */
		int iRows = objReport.getRowCount();
		iRows -= iNumLinesTitlePage;
		while (iRows > 0) {
			iNumPages += 1;
			iRows -= iNumLinesNormalPage;
		}
		/*
		 * restore graphics
		 */
		obj2DGraphics.setTransform(oldTrans);
		bInfoCalculated = true;
	}

	/*
	 * reduces header font size until column header text fits into column width
	 */

	private void scaleHeaderFont(Graphics2D obj2DGraphics) {
		Font fntHeader = objFonts.getHeaderFont();
		float fHeaderFontSize = fntHeader.getSize();
		boolean bUpdate = false;
		FontMetrics fm = obj2DGraphics.getFontMetrics(fntHeader);
		for (int ii = 0; ii < arriColumnWidths.length; ii++) {
			String strColumnName = objReport.getColumnNameNoHTML(ii);
			if (!StringUtils.isBlank(strColumnName)) {
				String[] arrstrLines = strColumnName.split("\n");
				iHeaderRows = Math.max(iHeaderRows, arrstrLines.length);
				for (String strLine : arrstrLines)
					if (!StringUtils.isBlank(strLine))
						while ((fm.stringWidth(strLine) > arriColumnWidths[ii])
								&& (fHeaderFontSize > 5.0F)) {
							fHeaderFontSize -= 0.5F;
							fntHeader = fntHeader.deriveFont(fHeaderFontSize);
							fm = obj2DGraphics.getFontMetrics(fntHeader);
							bUpdate = true;
						}
			}
		}
		if (bUpdate)
			objFonts.updateHeaderFontSize(fHeaderFontSize);
	}

}
