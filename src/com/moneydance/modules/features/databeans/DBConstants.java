package com.moneydance.modules.features.databeans;

public abstract class DBConstants {
	public static final String ACCOUNTBEAN = "account";
	public static final String ACCOUNTTYPEBEAN = "accounttype";
	public static final String CURRENCYBEAN = "currency";
	public static final String ADDRESSBEAN = "address";
	public static final String TRANSACTIONBEAN = "transaction";
	public static final String BUDGETBEAN = "budget";
	public static final String REPORTNAME = "report";

}
