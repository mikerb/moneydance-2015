package com.moneydance.modules.features.databeans;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.reflect.TypeToken;
import com.infinitekind.moneydance.model.Account;

public class AccountBean extends DataBean{
@Expose 	private String accountName;
@Expose 	private String accountType;
@Expose 	private String accountDescription;
@Expose 	private long annualFee;
@Expose 	private double apr;
@Expose 	private double aprPercent;
@Expose 	private String bankAccountNumber;
@Expose 	private String bankName;
@Expose 	private String broker;
@Expose 	private String brokerPhone;
@Expose 	private int cardExpirationMonth;
@Expose 	private int cardExpirationYear;
@Expose 	private String cardNumber;
@Expose 	private long creditLimit;
@Expose 	private String currencyTypeID;
@Expose 	private String currencyTypeName;
@Expose 	private String defaultCategory;
@Expose 	private String fullAccountName;
	/*
	 * Transient fields
	 */
	private transient Account account;
	
	public AccountBean () {
		super();
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getAccountDescription() {
		return accountDescription;
	}

	public void setAccountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
	}

	public long getAnnualFee() {
		return annualFee;
	}

	public void setAnnualFee(long annualFee) {
		this.annualFee = annualFee;
	}

	public double getApr() {
		return apr;
	}

	public void setApr(double apr) {
		this.apr = apr;
	}

	public double getAprPercent() {
		return aprPercent;
	}

	public void setAprPercent(double aprPercent) {
		this.aprPercent = aprPercent;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBroker() {
		return broker;
	}

	public void setBroker(String broker) {
		this.broker = broker;
	}

	public String getBrokerPhone() {
		return brokerPhone;
	}

	public void setBrokerPhone(String brokerPhone) {
		this.brokerPhone = brokerPhone;
	}

	public int getCardExpirationMonth() {
		return cardExpirationMonth;
	}

	public void setCardExpirationMonth(int cardExpirationMonth) {
		this.cardExpirationMonth = cardExpirationMonth;
	}

	public int getCardExpirationYear() {
		return cardExpirationYear;
	}

	public void setCardExpirationYear(int cardExpirationYear) {
		this.cardExpirationYear = cardExpirationYear;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public long getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(long creditLimit) {
		this.creditLimit = creditLimit;
	}

	public String getCurrencyTypeID() {
		return currencyTypeID;
	}

	public void setCurrencyTypeID(String currencyTypeID) {
		this.currencyTypeID = currencyTypeID;
	}
	public String getCurrencyTypeName() {
		return currencyTypeName;
	}

	public void setCurrencyTypeName(String currencyTypeName) {
		this.currencyTypeName = currencyTypeName;
	}

	public String getDefaultCategory() {
		return defaultCategory;
	}

	public void setDefaultCategory(String defaultCategory) {
		this.defaultCategory = defaultCategory;
	}

	public String getFullAccountName() {
		return fullAccountName;
	}

	public void setFullAccountName(String fullAccountName) {
		this.fullAccountName = fullAccountName;
	}
	public static DataBean deserialize(String beanData) {
		AccountBean bean;
		Type listType = new TypeToken<AccountBean>(){}.getType();
		bean = new Gson().fromJson(beanData,listType);
		return bean;
	}
	public String serialize(DataBean bean) {
		return new Gson().toJson(bean);
	}

	public void addAccount(Account account) {
		this.account = account;
	}
	
	public Account retrieveAccount() {
		return account;
	}

	@Override
	public void populateData() {
		setAccountName(account.getAccountName());
		setAccountType(account.getAccountType().name());
		setAccountDescription(account.getAccountDescription());
		setAnnualFee(account.getAnnualFee());
		setApr(account.getAPR());
		setAprPercent(account.getAPRPercent());
		setBankAccountNumber(account.getBankAccountNumber());
		setBankName(account.getBankName());
		setBroker(account.getBroker());
		setBrokerPhone(account.getBrokerPhone());
		setCardExpirationMonth(account.getCardExpirationMonth());
		setCardExpirationYear(account.getCardExpirationYear());
		setCardNumber(account.getCardNumber());
		setCurrencyTypeID(account.getCurrencyType().getIDString());
		setCurrencyTypeName(account.getCurrencyType().getName());
		setDefaultCategory(account.getDefaultCategory().getAccountName());
		setFullAccountName(account.getFullAccountName());
	}
	

}

