package com.moneydance.modules.features.forecaster;

public interface EntryListener {
	public abstract void tabEntered();
}
