package com.moneydance.modules.features.forecaster;

public interface PanelSize {
	public abstract void resize(int iWidth, int iDepth);

}
