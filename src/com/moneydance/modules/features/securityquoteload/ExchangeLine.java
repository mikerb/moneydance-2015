package com.moneydance.modules.features.securityquoteload;

public class ExchangeLine {
	private String exchange;
	private String name;
	private String ftPrefix;
	private String ftSuffix;
	private String yahooPrefix;
	private String yahooSuffix;
	private Double multiplier;
	public ExchangeLine () {
		
	}
	/**
	 * @return the exchange
	 */
	public String getExchange() {
		if (exchange == null)
			return "unknown";
		else
			return exchange;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		if (name == null)
			return "unknown";
		else
			return name;
	}
	/**
	 * @return the ftPrefix
	 */
	public String getFtPrefix() {
		if (ftPrefix == null)
			return "unknown";
		else
			return ftPrefix;
	}
	/**
	 * @return the ftSuffix
	 */
	public String getFtSuffix() {
		if (ftSuffix == null)
			return "unknown";
		else
			return ftSuffix;
	}
	/**
	 * @return the yahooPrefix
	 */
	public String getYahooPrefix() {
		if (yahooPrefix == null)
			return "unknown";
		else
			return yahooPrefix;
	}
	/**
	 * @return the yahooSuffix
	 */
	public String getYahooSuffix() {
		if (yahooSuffix == null)
			return "unknown";
		else
			return yahooSuffix;
	}
	/**
	 * @return the multiplier
	 */
	public Double getMultiplier() {
		if (exchange == null)
			return 0.0;
		else
			return multiplier;
	}
	/**
	 * @param exchange the exchange to set
	 */
	public void setExchange(String exchange) {
		this.exchange = exchange;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @param ftPrefix the ftPrefix to set
	 */
	public void setFtPrefix(String ftPrefix) {
		this.ftPrefix = ftPrefix;
	}
	/**
	 * @param ftSuffix the ftSuffix to set
	 */
	public void setFtSuffix(String ftSuffix) {
		this.ftSuffix = ftSuffix;
	}
	/**
	 * @param yahooPrefix the yahooPrefix to set
	 */
	public void setYahooPrefix(String yahooPrefix) {
		this.yahooPrefix = yahooPrefix;
	}
	/**
	 * @param yahooSuffix the yahooSuffix to set
	 */
	public void setYahooSuffix(String yahooSuffix) {
		this.yahooSuffix = yahooSuffix;
	}
	/**
	 * @param multiplier the multiplier to set
	 */
	public void setMultiplier(Double multiplier) {
		this.multiplier = multiplier;
	}
}
