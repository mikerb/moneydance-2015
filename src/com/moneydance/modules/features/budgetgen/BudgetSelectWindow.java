/*
 * Copyright (c) 2014, Michael Bray. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - The name of the author may not used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.moneydance.modules.features.budgetgen;

import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.prefs.Preferences;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.moneydance.awt.AwtUtil;
import com.moneydance.awt.GridC;

/*
 * Window to select the budget to be processes, note only budgets in new format are displayed
 * 
 * Old format has a PeriodType of 'Mixed'
 */
public class BudgetSelectWindow extends javax.swing.JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Main extension;
	private BudgetListExtend objBudgetList;
	@SuppressWarnings("rawtypes")
	private JComboBox boxBudget;
	private JPanel panScreen;
	private BudgetValuesWindow panValues = null;
	private BudgetValuesWindow panIncomeValues = null;
	private JFrame frmExpense;
	private JFrame frmIncome;
	private JButton btnValues;
	private JButton btnIncomeValues;
	private JButton btnClose;
	private JButton btnChoose;
	private JTextField txtFileName;
	@SuppressWarnings("unused")
	private LookAndFeel previousLF;
	private JFileChooser objFileChooser = null;
	private File fParameters;
	public boolean bError;
	/*
	 * Preferences
	 */
	private Preferences objPref;
	private Preferences objRoot;
	private String strFileName;
	private String strBudgetName;
	private String strFileNameNode = Constants.FILENAMEPREF;
	private String strBudgetNameNode = Constants.BUDGETNAMEPREF ;

	/**
	 * Creates new form BudgetMainWindow
	 */
	public BudgetSelectWindow(Main ext) {
		super("Budget Generator");
		bError = false;
		previousLF = UIManager.getLookAndFeel();
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (IllegalAccessException | UnsupportedLookAndFeelException
				| InstantiationException | ClassNotFoundException e) {
		}
		objFileChooser = new JFileChooser();
		this.extension = ext;
		setPreferences();
		GridBagLayout gbl_panScreen = new GridBagLayout();
		gbl_panScreen.columnWeights = new double[] { 0.0, 1.0 };
		panScreen = new JPanel(gbl_panScreen);
		panScreen.setBorder(new EmptyBorder(10, 10, 10, 10));

		// Budget
		JLabel lblAccountsName = new JLabel("Budget:");
		panScreen.add(lblAccountsName,
				GridC.getc(0, 0).west().fillx().insets(10, 10, 10, 10));

		// Select Budget
		objBudgetList = new BudgetListExtend(extension.getUnprotectedContext());
		String strNames[] = objBudgetList.getBudgetNames();
		if (strNames.length < 1){
			JFrame fTemp = new JFrame();
			JOptionPane
			.showMessageDialog(fTemp,
					"No Budgets have been created.  Use Tools/Budget Manager to create a budget before using this extension");
			bError = true;
			return;
		}
		boxBudget = new JComboBox<String>(strNames);
		boxBudget.setToolTipText("Select the budget you wish to work with");
		String strBudgettemp = objBudgetList.getBudgetKey (strBudgetName);
		if (strBudgettemp == Constants.NOBUDGET)
			strBudgetName = "";
		if (strBudgetName != "")
			boxBudget.setSelectedItem(strBudgetName);
		else {
			strBudgetName = strNames[0];
			updatePreferences(strFileName, strBudgetName);
		}
		panScreen.add(boxBudget,
				GridC.getc(1, 0).west().fillx().insets(10, 10, 10, 10));
		boxBudget.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				strBudgetName = (String) boxBudget.getSelectedItem();
				if (strFileName == "") {
					strFileName = objBudgetList.getBudgetKey(strBudgetName);
					txtFileName.setText(strFileName);
				}
				updatePreferences(strFileName, strBudgetName);
			}
		});		/*
		 * parameter file
		 */
		JLabel lblFileName = new JLabel("Parameters : ");
		panScreen.add(lblFileName,
				GridC.getc(0, 1).fillx().east().insets(10, 10, 10, 10));

		txtFileName = new JTextField();
		txtFileName.setColumns(20);
		txtFileName.setText(strFileName);
		txtFileName.setToolTipText("Enter the file name (without extension), or click on button to the right to find the file");
		txtFileName.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void changedUpdate(DocumentEvent e) {
				strFileName = txtFileName.getText();
			}
			@Override
			public void removeUpdate(DocumentEvent e) {
				strFileName = txtFileName.getText();
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				strFileName = txtFileName.getText();
			}
		});
		panScreen.add(txtFileName, GridC.getc(1, 1).fillx().west().colspan(3)
				.insets(10, 10, 10, 10));

		btnChoose = new JButton();
		Image img = getIcon("Search-Folder-icon.jpg");
		if (img == null)
			btnChoose.setText("Find");
		else
			btnChoose.setIcon(new ImageIcon(img));
		panScreen.add(btnChoose, GridC.getc(4, 1).fillx()
				.insets(10, 10, 10, 10));
		btnChoose.setBorder(javax.swing.BorderFactory
				.createLineBorder(panScreen.getBackground()));
		btnChoose.setToolTipText("Click to open file dialog to find required file");
		btnChoose.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				chooseFile();
			}
		});
		// Buttons
		btnValues = new JButton("Enter Expense Values");
		btnValues.setToolTipText("Click to display expense categories");
		btnValues.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				enterValues();
			}
		});

		panScreen.add(btnValues, GridC.getc(0, 2).insets(10, 10, 10, 10));

		btnIncomeValues = new JButton("Enter Income Values");
		btnIncomeValues.setToolTipText("Click to display income categories");
		btnIncomeValues.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				enterIncomeValues();
			}
		});

		panScreen.add(btnIncomeValues, GridC.getc(1, 2).insets(10, 10, 10, 10));
		btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				closeConsole();
			}
		});
		panScreen.add(btnClose, GridC.getc(2, 2).insets(10, 10, 10, 10));
		getContentPane().add(panScreen);
		pack();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		enableEvents(WindowEvent.WINDOW_CLOSING);

		AwtUtil.centerWindow(this);

	}

	/*
	 * Select a file
	 */
	private void chooseFile() {
		objFileChooser.setFileFilter(new FileNameExtensionFilter("Budget Files","bpic",
				"BPIC", "bpex", "BPEX"));
		objFileChooser.setCurrentDirectory(extension.getUnprotectedContext()
				.getCurrentAccountBook().getRootFolder());
		int iReturn = objFileChooser.showDialog(this, "Select File");
		if (iReturn == JFileChooser.APPROVE_OPTION) {
			fParameters = objFileChooser.getSelectedFile();
			txtFileName.setText(fParameters.getName().substring(0,
					fParameters.getName().lastIndexOf('.')));
		}
		updatePreferences(strFileName, strBudgetName);
	}

	private Image getIcon(String icon) {
		try {
			ClassLoader cl = getClass().getClassLoader();
			java.io.InputStream in = cl
					.getResourceAsStream("/com/moneydance/modules/features/budgetgen/"
							+ icon);
			if (in != null) {
				ByteArrayOutputStream bout = new ByteArrayOutputStream(1000);
				byte buf[] = new byte[256];
				int n = 0;
				while ((n = in.read(buf, 0, buf.length)) >= 0)
					bout.write(buf, 0, n);
				return Toolkit.getDefaultToolkit().createImage(
						bout.toByteArray());
			}
		} catch (Throwable e) {
		}
		return null;
	}

	public void closeConsole() {
		if (panValues != null) {
			panValues.setVisible(false);
			JFrame ValueFrame = (JFrame) SwingUtilities
					.getWindowAncestor(panValues);
			ValueFrame.dispose();
		}
		if (panIncomeValues != null) {
			panIncomeValues.setVisible(false);
			JFrame ValueFrame = (JFrame) SwingUtilities
					.getWindowAncestor(panIncomeValues);
			ValueFrame.dispose();
		}
		this.dispose();
		this.extension.closeConsole();
	}

	/*
	 * Display the detail based on selected options
	 */
	protected void enterValues() {
		// Create and set up the window.
		if (strBudgetName.equals("")) {
			JFrame fTemp = new JFrame();
			JOptionPane
			.showMessageDialog(fTemp,
					"No Budget Selected");
		}
		else {
			frmExpense = new JFrame(
					"MoneyDance Budget Generator - Enter Expense Values - Build "+Main.strBuild);
			frmExpense.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			panValues = new BudgetValuesWindow(this.extension,
					strBudgetName, Constants.EXPENSE_SCREEN, strFileName);
			frmExpense.getContentPane().add(panValues);
	
			// Display the window.
			frmExpense.getContentPane().setPreferredSize(
					new Dimension(panValues.iFRAMEWIDTH, panValues.iFRAMEDEPTH));
			frmExpense.pack();
			if (Main.imgIcon != null)
				frmExpense.setIconImage(Main.imgIcon);
			frmExpense.setVisible(true);
		}
	}

	protected void enterIncomeValues() {
		// Create and set up the window.
		if (strBudgetName.equals("")) {
			JFrame fTemp = new JFrame();
			JOptionPane
			.showMessageDialog(fTemp,
					"No Budget Selected");
		}
		else {
			frmIncome = new JFrame(
					"MoneyDance Budget Generator - Enter Income Values- Build "+Main.strBuild);
			frmIncome.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			panIncomeValues = new BudgetValuesWindow(this.extension,
					strBudgetName, Constants.INCOME_SCREEN, strFileName);
			frmIncome.getContentPane().add(panIncomeValues);
	
			// Display the window.
			frmIncome.getContentPane().setPreferredSize(
					new Dimension(panIncomeValues.iFRAMEWIDTH,
							panIncomeValues.iFRAMEDEPTH));
			frmIncome.pack();
			if (Main.imgIcon != null)
				frmIncome.setIconImage(Main.imgIcon);
			frmIncome.setVisible(true);
		}
	}


	void goAway() {
		if (panValues != null) {
			panValues.close();
			panValues = null;
		}
		if (panIncomeValues != null) {
			panIncomeValues.close();
			panIncomeValues = null;
		}
		if (frmExpense != null) {
			frmExpense.dispose();
			frmExpense = null;
		}
		if (frmIncome != null) {
			frmIncome.dispose();
			frmIncome = null;
		}
		setVisible(false);
		dispose();
	}

	/*
	 * preferences
	 */
	private void setPreferences() {
		objRoot = Preferences.userRoot();
		objPref = objRoot.node(Constants.PREFERENCESNODE);
		strBudgetName = objPref.get(strBudgetNameNode, "");
		strFileName = objPref.get(strFileNameNode, strBudgetName);

	}

	public void updatePreferences(String strFileNamep, String strBudgetNamep) {
		objPref.put(strFileNameNode, strFileNamep);
		objPref.put(strBudgetNameNode, strBudgetNamep);
	}
}
