package com.moneydance.modules.features.jasperreports;

public class TemplateRow {
	private String name;
	private String fileName;
	private String lastVerified;
	public TemplateRow() {
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getLastVerified() {
		return lastVerified;
	}
	public void setLastVerified(String lastVerified) {
		this.lastVerified = lastVerified;
	}

}
