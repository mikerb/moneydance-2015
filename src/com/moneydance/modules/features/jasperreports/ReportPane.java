package com.moneydance.modules.features.jasperreports;

import com.moneydance.modules.features.mrbutil.MRBDebug;
import com.moneydance.modules.features.mrbutil.MRBPreferences;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;

public class ReportPane extends ScreenPanel {
	private Parameters params;
	private ObservableList model;
    private TableView thisTable;
	private int[] columnWidths;
	private MRBDebug debugInst = MRBDebug.getInstance();
    private MRBPreferences preferences;
    private int SCREENWIDTH; 
    private int SCREENHEIGHT; 
	public ReportPane(Parameters paramsp) {
		params = paramsp;
		setUpTable();
		Label templateLbl = new Label("Reports");
		templateLbl.setTextAlignment(TextAlignment.CENTER);
		templateLbl.setFont(Font.font("Veranda",FontWeight.BOLD,20.0));
		templateLbl.setPadding(new Insets(10,10,10,10));
		add(templateLbl,0,0);
		GridPane.setHalignment(templateLbl, HPos.CENTER);
		add(thisTable,0,1);
		preferences = MRBPreferences.getInstance();
		resize();
	}
	
	public void resize() {
		SCREENWIDTH =preferences.getInt(Constants.PROGRAMNAME+"."+Constants.DATAPANEWIDTH,Constants.DATASCREENWIDTH);
		SCREENHEIGHT =preferences.getInt(Constants.PROGRAMNAME+"."+Constants.DATAPANEHEIGHT,Constants.DATASCREENHEIGHT);
		setPrefSize(SCREENWIDTH,SCREENHEIGHT);
		thisTable.setPrefWidth(SCREENWIDTH);
		thisTable.setPrefHeight(SCREENHEIGHT);
	}
	
	private void setUpTable () {
		thisTable = new TableView();
		thisTable.setEditable(true);
		/*
		 * Name
		 */
		TableColumn name = new TableColumn("Name");
		/*
		 * Ticker
		 */
		TableColumn lastVerified = new TableColumn("Last Verified Date");
		thisTable.getColumns().addAll(name,lastVerified);
		model =FXCollections.observableArrayList(params.getReportList());
		name.setCellValueFactory(new PropertyValueFactory<>("name"));
		lastVerified.setCellValueFactory(new PropertyValueFactory<>("lastVerified"));
	}


}
