package com.moneydance.modules.features.jasperreports;

public abstract class AbstractJasperRequest {
    public abstract String getTransactionId();

    public abstract String getShortSummary();

}
