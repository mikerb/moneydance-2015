package com.moneydance.modules.features.jasperreports;

import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;

public abstract class OptionMessage {
	
	public static void displayMessage(String message) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Information");
		alert.setHeaderText(null);
		alert.setContentText(message);
		alert.showAndWait();
	}
	public static boolean yesnoMessage(String message) {
		ButtonType yesButton = new ButtonType("Yes",ButtonBar.ButtonData.YES);
		ButtonType noButton = new ButtonType("No",ButtonBar.ButtonData.NO);
		Alert alert = new Alert(AlertType.CONFIRMATION,message,yesButton,noButton);
		alert.setTitle("Confirmation");
		alert.setHeaderText(null);
		alert.getButtonTypes().setAll(yesButton, noButton);
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == yesButton)
			return true;
		else
			return false;
	}
}
